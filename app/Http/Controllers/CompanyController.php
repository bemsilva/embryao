<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        try {
            $companies = Company::with('users')->get();
            return response()->json($companies, 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function show(Company $company)
    {
        try {
            return response()->json($company->load('users'), 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $company = Company::create($request->all());
            return response()->json($company, 201);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function update(Request $request, Company $company)
    {
        try {
            $company->update($request->all());
            return response()->json($company, 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function destroy(Company $company)
    {
        try {
            $company->delete();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }
}
