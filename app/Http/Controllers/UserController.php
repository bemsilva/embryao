<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        try {
            $users = User::with('companies')->get();
            return response()->json($users, 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }

    }

    public function show(User $user)
    {
        try {
            return response()->json($user->load('companies'), 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }

    }

    public function store(Request $request)
    {
        try {
            $user = User::create($request->all());
            return response()->json($user, 201);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function update(Request $request, User $user)
    {
        try {
            $user->update($request->all());
            return response()->json($user, 200);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();
            return response()->json(null, 204);
        } catch (\Exception $e) {
            //Change the return for the one below for a development version
            //return response()->json($e->getMessage(), 500);
            return response()->json('Could not handle your request right now. try again later.', 500);
        }
    }
}
