## Embryao

To run the project, setup a new homestead following the laravel homestead guidelines:
https://laravel.com/docs/5.8/homestead

At the project creation step, clone this repository to your projects folder (ex.: /home/vagrant/) 

```
composer install
```

```
npm install
```

```
npm run dev
```

```
php artisan migrate:fresh --seed
```

## URLs

The program was made while using the local url "http://embryao.local". feel free to use as you want, but remember to update the postman urls.

## Postman

The postman folder contains the tests for the api endpoints. Check the documentation upon importing the collections to your postman app for more info.
