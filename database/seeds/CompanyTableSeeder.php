<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => Str::random(10),
            'register' => Str::random(10),
            'address' => Str::random(20),
        ]);

        DB::table('companies')->insert([
            'name' => Str::random(10),
            'register' => Str::random(10),
            'address' => Str::random(20),
        ]);

        DB::table('companies')->insert([
            'name' => Str::random(10),
            'register' => Str::random(10),
            'address' => Str::random(20),
        ]);
    }
}
