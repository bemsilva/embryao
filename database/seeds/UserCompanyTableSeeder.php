<?php

use Illuminate\Database\Seeder;

class UserCompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_companies')->insert([
            'user_id' => 1,
            'company_id' => 2,
        ]);

        DB::table('users_companies')->insert([
            'user_id' => 1,
            'company_id' => 1,
        ]);

        DB::table('users_companies')->insert([
            'user_id' => 2,
            'company_id' => 2,
        ]);
    }
}
