<br><hr>
                    <div class="row">
                        <div class="col-md-6 offset-md-4">
                            <b>Current companies:</b><br><br>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Register</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->companies as $company)
                                    <tr>
                                        <th>{{$company->name}}</th>
                                        <th>{{$company->register}}</th>
                                    <th><a href="" class="btn btn-primary btn-block">X</a></th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6  offset-md-4">
                            <select name="" id="" class="form-control">
                                @foreach (\App\Company::all() as $company)
                                <option value="">{{$company->name}}</option>
                                @endforeach
                            </select><br>
                            <a href="" class="btn btn-primary btn-block">Add Company</a>
                        </div>
                    </div>
