@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">EDIT</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('users.update', $user->id) }}">
                        @csrf

                        <input name="_method" type="hidden" value="PUT">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                            <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>
                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{$user->username}}" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>
                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="email" value="{{$user->email}}" required>
                                </div>
                            </div>
                        <div class="form-group row">
                            <label for="document" class="col-md-4 col-form-label text-md-right">Identification</label>
                            <div class="col-md-6">
                                <input id="document" type="text" class="form-control" name="document" value="{{$user->document}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" name="address" value="{{$user->address}}" required>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">password</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="{{$user->password}}" required>
                                </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Save Changes
                                </button>
                                <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger btn-block">
                                    Delete
                                </a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
