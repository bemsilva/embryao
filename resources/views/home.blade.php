@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Companies</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <th>{{$user->name}}</th>
                                <th>{{$user->email}}</th>
                                <th>{{$user->username}}</th>
                                <th>
                                    @foreach ($user->companies as $company)
                                    {{$company->name}}
                                    @endforeach
                                </th>
                                <th><a class="btn btn-primary btn-small" href="{{route('user.edit.page', $user->id)}}">Edit User</a></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Register</th>
                                            <th>Address</th>
                                            <th>Users</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($companies as $company)
                                        <tr>
                                            <th>{{$company->name}}</th>
                                            <th>{{$company->register}}</th>
                                            <th>{{$company->address}}</th>
                                            <th>
                                                @foreach ($company->users as $user)
                                                {{$user->name}} <br>
                                                @endforeach
                                            </th>
                                            <th><a class="btn btn-primary btn-small" href="{{route('user.edit.page', $user->id)}}">Edit User</a></th>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
